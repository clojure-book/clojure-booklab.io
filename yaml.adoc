=== YAML

NOTE: Find the project source here https://gitlab.com/clojure-book/file-ops/-/tree/yaml_ops

YAML is lighter form of JSON and is often used for configuration files. In this section we will see how to use YAML in Clojure. First let's add the library `clj-yaml` to our project, as shown in the line `[clj-commons/clj-yaml "1.0.29"]`.

.project.clj
[source, clojure]
----
(defproject file-ops "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [org.clojure/data.csv "1.1.0"]
                 [org.clojure/data.json "2.5.1"]
                 [clj-commons/clj-yaml "1.0.29"]]
  :repl-options {:init-ns file-ops.core})
----

This will pull the necessary YAML library to use YAML in our project.

Now let's create a file called `yaml_ops.clj` with the content as shown below:

[source, clojure]
----
(ns file-ops.yaml-ops
  (:require [clj-yaml.core :as yaml]))
----

`(:require [clj-yaml.core :as yaml])`, requires the YAML library which can be referenced using the name `yaml`.

Rather than reading some YAML content from file, I should like to have it as a variable, so let's create a variable called `some-yaml` with the following content:

[source, clojure]
----
(def some-yaml "
todo:
  issues:
    - name: Fix all the things
      responsible:
        name: Rita
")
----

Now let's convert the content of `some-yaml` to a map using `(yaml/parse-string some-yaml)`:

[source, clojure]
----
(def some-data (yaml/parse-string some-yaml))

some-data
----

Output

[source, clojure]
----
{:todo {:issues [{:name "Fix all the things", :responsible {:name "Rita"}}]}}
----

If you see above, all the keys are keywords, if you don't want such behavior, pass `:keywords false` as shown below:

[source, clojure]
----
(yaml/parse-string some-yaml :keywords false)
----

Output

[source, clojure]
----
{"todo" {"issues" ({"name" "Fix all the things", "responsible" {"name" "Rita"}})}}
----

This will make keywords as string as shown above.

Now let's print the data in YAML format using `(println (yaml/generate-string some-data))`

[source, clojure]
----
(println (yaml/generate-string some-data :dumper-options {:indent 2
                                                 :flow-style :block}))
----

Output

[source, yaml]
----
todo:
  issues:
  - name: Fix all the things
    responsible:
      name: Rita
----

Note above that we have set the indentation to 2 and flow-style to block, so that the output will look very neat and is human-readable. Without these options, the output will look like this:

[source, clojure]
----
(println (yaml/generate-string some-data))
----

Output

[source, yaml]
----
todo:
  issues:
  - name: Fix all the things
    responsible: {name: Rita}
----

The entire yaml operations are listed below, you can find the file here https://gitlab.com/clojure-book/file-ops/-/raw/yaml_ops/src/file_ops/yaml_ops.clj

.https://gitlab.com/clojure-book/file-ops/-/raw/yaml_ops/src/file_ops/yaml_ops.clj[src/file_ops/yaml_ops.clj]
[source, clojure]
----
;; yaml_ops.clj
;; https://github.com/clj-commons/clj-yaml/blob/master/doc/01-user-guide.adoc

(ns file-ops.yaml-ops
  (:require [clj-yaml.core :as yaml]))

(def some-yaml "
todo:
  issues:
    - name: Fix all the things
      responsible:
        name: Rita
")

(def some-data (yaml/parse-string some-yaml))

some-data

(yaml/parse-string some-yaml :keywords false)

(println (yaml/generate-string some-data :dumper-options {:indent 2
                                                 :flow-style :block}))

(println (yaml/generate-string some-data))
----
